import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Category } from '../models/Category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http:HttpClient) { }
  categoriesUrl:string = 'https://jsonplaceholder.typicode.com/todos/';
  categoriesLimit:string = '?_limit=10';

  getCategories():Observable<Category[]>{

    return this.http.get<Category[]>(`${this.categoriesUrl}${this.categoriesLimit}`);
  }
}
