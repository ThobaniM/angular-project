import { Pipe, PipeTransform } from '@angular/core';
import { Log } from '../models/Log';

@Pipe({
  name: 'searchOnTerm'
})
export class SearchOnTermPipe implements PipeTransform {

  // transform(value: unknown, ...args: unknown[]): unknown {
  //   return null;
  // }

  transform(value: Log[], searchTerm?: string): Log[] {
    return value.filter(log => log.text.indexOf(searchTerm) >= 0);
  }

}
