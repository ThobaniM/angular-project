import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { AdditionComponent } from './components/addition/addition.component';
import { LogFormComponent } from './components/log-form/log-form.component';
import { LogsComponent } from './components/logs/logs.component';


const routes: Routes = [
  { path: '', component:HomeComponent },
  { path: 'categories', component:CategoriesComponent },
  { path: 'addition', component:AdditionComponent },
  { path: 'logs', component:LogFormComponent },
  { path: 'logs', component:LogsComponent, outlet:'secondary'}
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
