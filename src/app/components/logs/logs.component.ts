import { Component, OnInit, Injector } from '@angular/core';

import { LogService } from '../../services/log.service';

import { Log } from '../../models/Log';

import { GenericPageComponent } from '../log-form/log-form.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css'],
})
export class LogsComponent extends GenericPageComponent implements OnInit {
  logs: Log[];
  selectedLog: Log;
  loaded: boolean = false;
  searchTerm: string = '';
  nowDate: Date = new Date();

  constructor(private logService: LogService, private injector: Injector) {
    super();
  }

  ngOnInit() {
    this.logService.stateClear.subscribe((clear) => {
      if (clear) {
        this.selectedLog = { id: '', text: '', date: '' };
      }
    });

    this.logService.getLogs().subscribe((logs) => {
      this.logs = logs;
      this.loaded = true;
    });
  }

  onSelect(log: Log) {
    this.logService.setFormLog(log);
  }

  onDelete(log: Log) {
    if (confirm('Are you sure')) {
      this.logService.deleteLog(log);
    }
  }

  getTimerPicker() {
    this.time = this.time;
  }

  getDate(dateToday) {
    //this.dateToday = formatDate('2019-04-13T00:00:00', 'yyyy-MM-dd', 'en-US');
  }
}
