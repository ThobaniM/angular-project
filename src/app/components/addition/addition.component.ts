import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-addition',
  templateUrl: './addition.component.html',
  styleUrls: ['./addition.component.css']
})
export class AdditionComponent implements OnInit {

    answer: number;
    number1: number = 1;
    number2: number = 3;

    constructor(){ }
    
    addNumbers(number1: number, number2: number){
      this.answer = number1 + number2;
    }

    ngOnInit(): void {
      // TODO
    }
}


