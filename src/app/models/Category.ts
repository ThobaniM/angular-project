export class Category {
  id:number;
  title:string;
  enabled:boolean;
}